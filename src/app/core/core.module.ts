import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotfoundComponent } from './notfound/notfound.component';
import { MainComponent } from './main/main.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HttpClientModule } from '@angular/common/http';
import { IndexComponent } from './index/index.component';
import { CoreRoutingModule } from './core-routing.module';


@NgModule({
  declarations: [
    NotfoundComponent, 
    NavbarComponent,
    MainComponent,
    IndexComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    CoreRoutingModule
  ]
})
export class CoreModule { }
