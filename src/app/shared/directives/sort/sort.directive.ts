import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';
import { Sort } from '../../class/sort/sort';

@Directive({
  selector: '[directiveSort]'
})
/**
 * Directiva a aplicar en las casillas a filtrar por una propiedad específica.
 * @author José Camilo Jaimes Charry <jcjaimesc@unal.edu.co>
 * @version 20201122
 */
export class SortDirective {

  @Input() directiveSort: Array<any>;
  @Input() sortProperty: any;

  constructor(private renderer: Renderer2, private targetElement: ElementRef) { }

  @HostListener("click")
  /**
   * Metodo que permite aplicar el sort a la columna seleccionada por el usuario.
   */
  filterData() {
    console.log('funca');
    const sort = new Sort();
    const element = this.targetElement.nativeElement;
    const order = element.getAttribute('sort-order');
    const property = this.sortProperty;

    if (order === 'desc') {
      this.directiveSort.sort(sort.sortByProperty(property, order))
      element.setAttribute('sort-order', 'asc');
    } else {
      this.directiveSort.sort(sort.sortByProperty(property, order))
      element.setAttribute('sort-order', 'desc');
    }
  }

}
