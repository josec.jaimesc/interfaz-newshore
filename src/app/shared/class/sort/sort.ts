/**
 * Clase que contiene la lógica de un ordenamiento por propieades sobre listas de objetos.
 * @author José Camilo Jaimes Charry <jcjaimesc@unal.edu.co>
 * @version 20201122
 */
export class Sort {
    /**
     * Estado del orden, desc (1) asc (-1)
     */
    private order: any = 1;
    /**
     * Comparador de strings
     */
    private filter = new Intl.Collator(undefined, {
        sensitivity: 'base',
    });

    constructor() {

    }
    /**
     * Metodo que permite aplicar un ordenamiento a la propiedad seleccionada por el usuario
     * @param property - nombre de la propiedad a filtrar
     * @param order - el estado del orden, desc o asc
     */
    public sortByProperty(property: any, order: any) {
        if (order === 'desc') {
            this.order = -1;
        }
        return (a, b) => {
            return this.filter.compare(a[property], b[property]) * this.order;
        }
    }
}
