import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
/**
 * Servicio que maneja las peticiones al endpoint de Houses
 * @author José Camilo Jaimes Charry <jcjaimesc@unal.edu.co>
 * @version 20201121
 */
export class HousesService {

  private url: string = 'https://hp-api.herokuapp.com/api/characters';

  constructor(private http: HttpClient) { }

  /**
   * Metodo que obtiene todas las casas (houses) del servicio
   */
  public allHouses() {
    return this.http.get<any[]>(this.url).pipe(map( data => data));
  }
}
