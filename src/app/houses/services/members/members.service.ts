import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
/**
 * Servicio que maneja las peticiones al endpoint de Members
 * @author José Camilo Jaimes Charry <jcjaimesc@unal.edu.co>
 * @version 20201122
 */
export class MembersService {

  private url: string = 'https://hp-api.herokuapp.com/api/characters/house';

  constructor(private http: HttpClient) { }

  /**
   * Metodo que obtiene todas las members (miembros) de las casas del servicio de la API.
   */
  public allMembers(house: string) {
    return this.http.get<any[]>(this.url + '/' + house).pipe(map( data => data));
  }
}
