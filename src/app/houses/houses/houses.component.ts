import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HousesService } from '../services/houses/houses.service';

@Component({
  selector: 'app-houses',
  templateUrl: './houses.component.html',
  styleUrls: ['./houses.component.sass'],
  providers: [
    HousesService,
  ]
})
/**
 * Componente que manipula la lógica de la funcionalidad de houses.
 * @author José Camilo Jaimes Charry <jcjaimesc@unal.edu.co>
 * @version 20201121
 */
export class HousesComponent implements OnInit {

  /**
   * Variable que almacena las respuestas de los servicios.
   */
  private answer: any;
  public houses: any;
  private colors: any;

  constructor(private HousesService: HousesService, private routing: Router,) {
    this.colors = [
      {'stroke': '#F1948A', 'color': '#F1948A'},
      {'stroke': '#C39BD3', 'color': '#C39BD3'},
      {'stroke': '#7FB3D5', 'color': '#7FB3D5'},
      {'stroke': '#73C6B6', 'color': '#73C6B6'},
      {'stroke': '#7DCEA0', 'color': '#7DCEA0'},
      {'stroke': '#F7DC6F', 'color': '#F7DC6F'},
      {'stroke': '#F8C471', 'color': '#F8C471'},
      {'stroke': '#D0D3D4', 'color': '#D0D3D4'},
      {'stroke': '#AAB7B8', 'color': '#AAB7B8'},
      {'stroke': '#34495E', 'color': '#34495E'},
    ];
  }

  ngOnInit(): void {
    this.getHouses();
  }

  /**
   * Metodo que permite obtener todas las casas (houses).
   */
  public getHouses() {
    this.HousesService.allHouses().subscribe(
      (data) => {
        this.answer = data;
        this.houses = this.getDistinctHouses(this.answer);
        console.log(this.answer);
        console.log(this.houses);
      },
      (error) => {
        console.log(error);
      }
    );
  }
  /**
   * Metodo que busca los diferentes nombres de casas y los agrupa en un arreglo de forma única.
   * @param houses - toda la información que se obtiene del servicio
   */
  private getDistinctHouses(houses: any) {
    const makeHouses = this.addHomelessAndMap(houses.map(item => item.house).filter((value, index, self) => self.indexOf(value) === index));
    return makeHouses;
  }

  /**
   * Metodo que permite contar la cantidad de miembros de una casa
   * @param house - nombre de la casa
   * @param allData - todos los datos del servicio
   */
  private countMembers(house: string, allData: any) {
    var countMembers = 0;
    allData.forEach(item => {
      if (item.house === house) {
        countMembers++;
      }
    });
    return countMembers;
  }
  /**
   * Metodo que permite generar una nueva estructura en el objeto de casas agregando las propiedades name, count y style.
   * Estas hacen referencia al nombre y la cantidad de miembros.
   * @param houses - nombres de las diferentes casas
   */
  private addHomelessAndMap(houses: any) {
    var makeHouses = [];
    const homeless = "Homeless";
    houses.forEach(house => {
      if (house === "") {
        makeHouses.push({
          name: homeless,
          count: this.countMembers("", this.answer),
          style: this.colors[Math.round(Math.random()*10)],
        })
      } else {
        makeHouses.push({
          name: house,
          count: this.countMembers(house, this.answer),
          style: this.colors[Math.round(Math.random()*10)],
        })
      }
    });
    return makeHouses;
  }

  /**
   * Metodo que permite ir a la ruta que lista los miembros de una casa
   * @param house - nombre de la casa seleccionada
   */
  public showMembers(house: string) {
    this.routing.navigate(['/houses/members', house]);
  }


}
