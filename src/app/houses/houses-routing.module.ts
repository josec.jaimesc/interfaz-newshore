import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule  } from '@angular/router';
import { MainComponent } from '../core/main/main.component';
import { HousesComponent } from './houses/houses.component';
import { MembersComponent } from './members/members.component';

const routes: Routes = [
  {
    path: 'houses',
    component: MainComponent,
    children: [
        { path: 'list', component: HousesComponent },
        { path: 'members/:name', component: MembersComponent },
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class HousesRoutingModule { }
