import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HousesComponent } from './houses/houses.component';
import { MembersComponent } from './members/members.component';
import { HousesRoutingModule } from './houses-routing.module';
import { SharedModule } from '../shared/shared.module';

/**
 * Librerias del modulo
 */
import { FontAwesomeModule, FaIconLibrary  } from '@fortawesome/angular-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { FormsModule } from '@angular/forms';
/**
 * Importación de los iconos de fontawesome
 */

@NgModule({
  declarations: [
    HousesComponent,
    MembersComponent,
  ],
  imports: [
    CommonModule,
    HousesRoutingModule,
    FontAwesomeModule,
    FormsModule,
    SharedModule
  ]
})
export class HousesModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas);
    library.addIconPacks(fab);
    library.addIconPacks(far);
  }
}
