import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MembersService } from '../services/members/members.service';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.sass'],
  providers: [
    MembersService
  ]
})
/**
 * Componente que manipula la lógica de la funcionalidad de los miembros de las casas.
 * @author José Camilo Jaimes Charry <jcjaimesc@unal.edu.co>
 * @version 20201122
 */
export class MembersComponent implements OnInit {

  /**
   * Contiene el nombre de la casa seleccionada
   */
  private house: string;
  /**
   * Contiene los miembros de la casa seleccionada
   */
  public members: any;
  /**
   * Contiene la respuesta original del servicio
   */
  private answer: any;
  /**
   * Contiene los encabezados de la tabla
   */
  public headers: any;

  constructor(private membersService: MembersService, private route: ActivatedRoute) {
    this.headers = [
      {
        name: 'Name',
        property: 'name'
      },
      {
        name: 'Last name',
        property: 'lastName'
      },
      {
        name: 'Ancestry',
        property: 'ancestry'
      },
      {
        name: 'Species',
        property: 'species'
      },
      {
        name: 'Gender',
        property: 'gender'
      },
    ];
  }

  ngOnInit(): void {
    this.getParameters();
    this.getMembers();
  }

  /**
   * Metodo que captura los parametros que llegan por url.
   */
  public getParameters() {
    this.house = this.route.snapshot.paramMap.get('name');

  }

  /**
   * Metodo que permite obtener todas las casas (houses).
   */
  public getMembers() {
    this.membersService.allMembers(this.removeHomeless(this.house)).subscribe(
      (data) => {
        this.answer = data;
        this.members = this.addLastname(data);
        console.log(this.answer);
      },
      (error) => {
        console.log(error);
      }
    );
  }
  /**
   * Metodo que permite separar el apellido del nombre, esto dado que los nombres solo estan compuestos por un nombre y apellido
   * @param members - lista de los miembros
   */
  private addLastname(members: any) {
    var makeMembers = [];
    members.forEach(member => {
      const breakName = member.name.split(' ')
      member.lastName = breakName[1];
      member.name = breakName[0];
      makeMembers.push(member);
    });
    return makeMembers;
  }
  /**
   * Al hacerse una conversión de las casas sin nombre por Homeless, se debe cambiar el parametro de busqueda para que el
   * servicio obtenga información
   * @param house - nombre de la casa
   */
  private removeHomeless(house) {
    if (house === 'Homeless') {
      return "";
    } else {
      return house;
    }
  }





}
